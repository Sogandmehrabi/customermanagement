<%@ page import="dto.RealCustomer" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>به روز رسانی</title>
    <link rel="stylesheet" type="text/css" href="css/register.css">
</head>
<body bgcolor="#E6E6FA">
<center>
<div id="div_form">

    <form name="regi" action="real-customer-list-update" method="post" accept-charset="UTF-8" dir="rtl">
       <div id="title"><h2>به روز رسانی مشتری</h2></div>

        <div> <label for="r6" id="cn">شماره مشتری </label> <input  type="text" name="customerNumber" value="${realCustomer.customerNumber}" readonly id="r6" style="margin-left: 20px"></div>
        </br>

        <div><label for="r1" id="fn">نام </label><input type="text" name="name"  value="${realCustomer.name}" id="r1">
            &nbsp &nbsp &nbsp <label for="r2" id="ln">نام خانوادگی </label><input type="text" name="lastName" value="${realCustomer.family}" id="r2"></div>
        <br/>


        <div> <label for="r3" id="fan">نام پدر </label><input type="text" name="fatherName" value="${realCustomer.fatherName}" id="r3" style="margin-right: 10px"></div>
        <br/>


        <div> <label for="r4" id="bd">تاریخ تولد </label><input type="text" name="birth" value="${realCustomer.birth}" id="r4"></div>
        <br/>


        &nbsp &nbsp &nbsp <div> <label for="r10" id="code">کد ملی </label><input type="text" name="code" id="r10" value="${realCustomer.code}" style="margin-right: 10px"></div>
        <br/>

        <div><label for="r7" id="ct">نوع مشتری </label> <input  type="text" name="customerType"  value="حقیقی" readonly id="r7" disabled> </div>
        </br>

        &nbsp &nbsp &nbsp <button type="submit" value="Submit" id="button">به روزرسانی مشتری</button>

    </form>

</div>
</center>
</body>
</html>
